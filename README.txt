CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


 INTRODUCTION
------------

Allow paiement within the global payement gateway GPG Checkout, 
This method is available for africa and the middle east. 
You can use it with Drupal Commerce.

REQUIREMENTS
------------

This module requires the following modules:

 * Commerce (https://drupal.org/project/commerce)
 * Commerce Paiement

 INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Enable paiement method in Admin > Store > Configuration > Paiements 
 * Configure the settings in the Actions area
 * This module requires a field_phone at least to work it can be set 
   in Billing informations : 
   admin/commerce/customer-profiles/types/billing/fields

MAINTAINERS
-----------
Current maintainers:
* utiks - https://www.drupal.org/u/utiks
